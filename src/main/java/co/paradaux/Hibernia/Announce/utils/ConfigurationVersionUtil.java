package co.paradaux.Hibernia.Announce.utils;

import org.bukkit.configuration.file.FileConfiguration;

public class ConfigurationVersionUtil {

	public double getConfigurationVersion(FileConfiguration file) {

		return Double.valueOf(file.getString("config-version"));

	}



}
